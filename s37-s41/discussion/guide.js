Session 37:
	touch index.js .gitignore guide.js
	npm init --y
	npm install express mongoose cors
	create our Course Model
	create our User Model

Session 38:
	create a route for checkEmail
	create a route for registerEmail
	npm install bcrypt
		use bcrypt in user s password
	npm install jsonwebtoken
	create a route for user authentication
	create a route for user details

Session 39:
	refactor the user details, added authorization
	create a route for add course, added authorization and only admin can add course

Session 40:
	create a route for retrieving all the courses
	create a route for retrieving all active courses
	create a route for retrieving a specific course
	create a route for updating a course
	create a route for archiving a course

Session 41:
	create a route for enroll a course
	

// It will load our jsonwebtoken package
const jwt = require("jsonwebtoken");

// Used in the algorithm for encrypting our data which makes it difficult to decode the information with the defined secret keyword
// User defined string data that will be used to create our JSON web tokens
const secret = "CourseBookingAPI";

// [ SECTION ] JSON Web Tokens
	// JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server

// Token Creation

module.exports.createAccessToken = (user) => {

	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information
	// payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
		fName: user.firstName
	};

	// Generate a JSON web token using the jwt's sign method
	// Generate the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
};

// Token Verification

module.exports.verify = (req, res, next) => {

	// The token is retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer Token
	let token = req.headers.authorization

	// Token received and is not undefined
	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		// The "slice" method takes only the token from the information sent via the request header
		// The token sent is a type of "Bearer" token which when received contains the word "Bearer " as a prefix to the string
		// This removes the "Bearer " prefix and obtains only the token for verification
		// Validate the token using the "verify" method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {

			// If JWT is not valid
			if(err){

				return res.send({ auth: "failed" });

			// If JWT is valid
			} else {

				// Allows the application to proceed with the next middleware function/callback function in the route
				next()
			}
		})

	// Token does not exist
	} else {

		return res.send({ auth: "failed" });
	};
};

// Token Decryption

module.exports.decode = (token) => {

	// Token received and is not undefined
	if(typeof token !== "undefined"){

		// Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err){

				return null;

			} else {

				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token, { complete: true }).payload;
			};
		})
	
	// Token does not exist
	} else {

		return null
	}

}
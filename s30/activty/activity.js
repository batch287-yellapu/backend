db.fruits.aggregate([
		{ $match: {onSale: true}},
		{$group: {_id: "onSale", fruitsOnSale: {$count: {}}}},
		{$project: { _id: 0}}
	]);

db.fruits.aggregate([
		{ $match: {stock: {$gte: 20}}},
		{$group: {_id: "stock", enoughStock: {$count: {}}}},
		{$project: { _id: 0}}
	]);

db.fruits.aggregate([
        { $match: {onSale: true}},
		{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
	]);

db.fruits.aggregate([
		{ $match: {onSale: true}},
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
		{$sort: {max_price: 1}}
	]);

db.fruits.aggregate([
		{ $match: {onSale: true}},
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
		{$sort: {min_price: 1}}
	]);
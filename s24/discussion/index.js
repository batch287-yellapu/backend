console.log("Hello Wurld?");

// [SECTION] Exponent Operator

	const firstNum = 8 ** 2;
	console.log(firstNum);

	const secondNum = Math.pow(8,2);
	console.log(secondNum);

// [SECTION] Template Literals

	// Allows us to write things without using the concatenation operator (+);

	let name = "John";

	// Uses single quotes(' ')
	let message = 'Hello ' + name + "! Welcome to programming!";
	console.log(message);

	// Strings Using Template Literal
	// Uses backticks (``)
	message = `Hello ${name}! Welcome to programming!`;
	console.log(`Message without template literals: ${message}`);

	// Multi-Line using Template Literals
	const anotherMessage = `
	${name} attended a math competition.
	He won it by solving the problem  8**2 with the solution of ${firstNum}.`

	console.log(anotherMessage);

		// Template literals allows us to write string with embedded JS expressions

	const interestRate = 0.1;
	const principal = 1000;

	console.log(`The interest on your saving account is: ${principal * interestRate }.`);

// [SECTION] Array Destructuring
	// Allows us to unpack elements in arrays into distinct variables

	// Syntax:
		// let/const [ variableName, variableName, variableName ] = array;

	const fullName = [ 'Juan', 'Dela', 'Cruz'];

	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)

	const [firstName1, middleName1, lastName1] = fullName;

	console.log(firstName1);
	console.log(middleName1);
	console.log(lastName1);

	console.log(`Hello ${firstName1} ${middleName1} ${lastName1}! It's nice to meet you!`);

// [SECTION] Object Destructuring
	// Allows to unoack properties of objects into distinct variables

	// Syntax:
		// let/const {propertyName, propertyName, propertyName } = object;

	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz' 

	};

	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!` );

	// Object Destructuring
	const {givenName, maidenName, familyName} = person;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!` );

	function getFullName ({givenName, maidenName, familyName}){
		console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
	};

	getFullName(person);
	console.log(person);

// [SECTION] Arrow Functions
	// Compact alternative syntax to traditional functions

	const hello = () => {
		console.log("Hellow wurld?");
	};

	// Mini-Activity

	const printFullName = (givenName, maidenName, familyName) =>{
		console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
	};

	printFullName(givenName, maidenName, familyName);
	printFullName(person); 

	// const printFullName = ({givenName, maidenName, familyName}) =>{
	// 	console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
	// };

	// printFullName(person);

	printFullName("John", 'D', 'Smith');

	const students = ['John', 'Jane', 'Judy'];

	// Arrow Functions with Loops
		// Pre-Arrow Function
	students.forEach(function(student){
		console.log(`${student} is present.`);
	});

	// Arrow Functions
	students.forEach((student) => {
		console.log(`${student} is absent.`);
	});

// [SECTION] Implicit Return Statement
	// There are instances when you can omit the 'return' statement

	// Pre-Arrow Function
	// const add = (x,y) => {
	// 	return x+y;
	// }
 	
 	// let total = add(1,2);
	// console.log(total);

	// Arrow Function
	const add = (x,y) => x+y;

	let total = add(1,2);
	console.log(total);

// [SECTION] Default Function Argument Value
	// Provides a defalut argument value if none is provided when the function is invoked

	const greet = (name = 'user') => {
		return `Good morning, ${name}!`;
	};

	console.log(greet());
	console.log(greet("John"));

// [SECTION] Class-Based Project Blueprints
	// Allows creation/instantiation of objects using classes as blueprints

	// Creating A Class
		// The constructor is a special method of a class for creating/initializing an object for that class.
		// A temmplate for a JS Object
		// Syntax:
			// class className{
			// 	constructor(objectPropertyA, objectPropertyB){
			// 		this.objectPropertyA = objectPropertyA;
			// 		this.objectPropertyB = objectPropertyB;
			// 	}
			// }

	class car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	// Instantiating an object
		// The "new"  operator creates/instantiates a new obeject with the given arguments as the value of its properties

	let myCar = new car();

	console.log(myCar);

		myCar.brand = "Ford";
		myCar.year = 2023;
		myCar.name = "Mustang";

	console.log(myCar);

	// car is class name

	const myNewCar = new car ('Toyota', 'Hilux', 2020);
	console.log(myNewCar);

	console.log(typeof myNewCar);
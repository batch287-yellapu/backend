let num1 = 2;
let getCube = num1 ** 3;
console.log(`The cube of ${num1} is ${getCube}`);

let address = ['258 Washington Ave NW', 'California', '90011'];
let [addressLine1, addressLine2, addressLine3] = address;
console.log(`I live at ${addressLine1}, ${addressLine2} ${addressLine3}`);

let animal = {
	// animal details
	name: 'Lolong',
	habitat: 'saltwater',
	animalType: 'crocodile',
	weight: 1075,
	measurement: '20 ft 3 in'
};

let {name, habitat, animalType, weight, measurement} = animal;
console.log(`${name} was a ${habitat} ${animalType}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);

let numbers = [1,2,3,4,5];
let printNumbers = numbers.forEach((number) => console.log(number));
// console.log(printNumbers);


let reduceNumber = numbers.reduce((x,y) => {
		return x + y;
	});

console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};

let myDog = new Dog();

myDog.name = 'Frankie';
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);
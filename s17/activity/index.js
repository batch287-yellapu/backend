/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userInfo(){
		let userName = prompt("What is your name?");
		let userAge = prompt("What is you age?");
		let userLocation = prompt("Where do you live?");

		console.log("Hello, " + userName);
		console.log("You are " + userAge + " years old.");
		console.log("You live in " + userLocation);

		alert("Thank you "+ userName + " for the information!");
	};

	userInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function topFavouriteMusicArtists(){
		console.log("1. Sid Sriram");
		console.log("2. A.R. Rahman");
		console.log("3. Harrdy Sandhu");
		console.log("4 Mangli");
		console.log("5. Neha Kakkar");
	}

	topFavouriteMusicArtists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favouriteMoviesRottenTomatoRatings(){
		console.log("1. RRR");
		console.log("Rotten Tomatoes Rating: 95%");
		console.log("2. Bahubali: The Beginning");
		console.log("Rotten Tomatoes Rating: 90%");
		console.log("3. Harry Potter and the Prisoner of Azkaban");
		console.log("Rotten Tomatoes Rating: 90%");		
		console.log("4. Harry Potter and the Deathly Hallows: Part 1");
		console.log("Rotten Tomatoes Rating: 77%");
		console.log("5. Beauty and the Beast");
		console.log("Rotten Tomatoes Rating: 71%");
	}

	favouriteMoviesRottenTomatoRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);

console.log('Hello World!');

//[SECTION] Arithmetic Operators

	let x = 5;
	let y = 28;

	console.log(x);
	console.log(y);

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

//[ SECTION ] Assignment Operators

	//Basic Assignment Operator (=)
	// This assignment operator assigns the value of the right operand to a variable and assigns the result to the variable
	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// The Addition assignment operator add the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber = assignmentNumber + 2;
	console.log("Result os addition assignment operator: " + assignmentNumber);

	// Shorthand for assignmentNumber = assignmentNumber + 2
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	// Subtraction/Multiplication/Division Assignment Operator(-=, *=, /=)

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Paranthese
	
	// PEMDAS Rule (Paranthesis, Exponents, Multipllication, Division, Addition and Subtraction)

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operator: " + mdas);

		// The operation follows:
		// 1. 3 * 4 = 12
		// 2. 12 / 5 = 2.4
		// 3. 1 + 2 = 3
		// 4. 3 - 2.4 = 0.6

	let pemdas = 1 + (2-3) * (4/5);
	console.log(pemdas);

	// Mini-Activity

		// The order of the operations:
		// 1. 2 - 3 = -1
		// 2. 4 / 5 = 0.8
		// 3. -1 * 0.8 = -0.8
		// 4. 1 + -0.8 = 0.2

// [SECTION] Increment and Decrement
	// Operators that adds or subtract values by 1 and reassigns the value of the variable wherethe increment/decrement was applied to

	let z = 1;

	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	increment = z++;
	// The value of "z" is at 2 before it was incremented
	console.log("Result of post-increment: " + increment);
	// The value of "z" was increased again reassigning the value to 3
	console.log("Result of post-increment: " + z);

	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);
	
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);

// [ SECTION ] Type Coercion
	// Is the automatic or implicit conversion of values from one data type to another.

	let numA = "10";
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof numA);
	console.log(typeof coercion);

	// Black text means that the output returned is a string data type.

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);
		// The boolean "true" is also associated with the value of 1.

	let numF = false + 1;
	console.log(numF);
		// The boolean "false" is also associated with the value of 0.

// [SECTION] Comparision Operator

	let juan = 'juan';

	// Equality Operator (==)
	/*
		- Cheks whether the operands are equal/have the same content
		- Attempts to COVERT and COMPARE
		- Returns a boolean value
	*/

	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log(0 == false);
	console.log('juan' == 'juan');
	console.log('juan' == juan);

	// Inequality Operator (!=)
	/*
		- Checks whether the operands are not equal/have different content
		- Attemts to CONVERT and COMPARE operands of different data types
	*/

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false);
	console.log('juan' != 'juan');
	console.log('juan' != juan);

	// Strict Equality Operator
	/*
		- Cheks whether the operands are equal/have the same content
		- Also COMPARES the data types of 2 values
		- Strict Equality Operators are better to use in most cases to ensure the data types are provided are correct
	*/

	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === '1');
	console.log(0 === false);
	console.log('juan' === 'juan');
	console.log('juan' === juan);

	// Strict Inequality Operator (!==)
	/*
		- Cheks whether the operands are not equal/have the same content
		- Also COMPARES the data types of 2 values
	*/

	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== '1');
	console.log(0 !== false);
	console.log('juan' !== 'juan');
	console.log('juan' !== juan);

// [ SECTION ] Relational Operators
	// Some comparision operators check whether one value is greater or less than to the other value.

	let a = 50;
	let b = 65;

	// GT orr Greater Than Operator ( > )
	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	// LT orr Less Than Operator ( < )
	let isLessThan = a < b;
	console.log(isLessThan);

	// GTE or Greater Than or Equal Operator ( >= )
	let isGTorEqual = a >= b;
	console.log(isGTorEqual);

	// GTE or Lower Than or Equal Operator ( <= )
	let isLTorEqual = a <= b;
	console.log(isLTorEqual);

// Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND Operator (&& - Double Ampersand)
	// Returns true is all operands are true.
	let allRequiremetsMet = isLegalAge && isRegistered;
	console.log("Result of Logical AND Operator: " + allRequiremetsMet);

	// Logical OR Operator (|| - Double Pipe)
	// Returns true if one of the operands is true.
	let SomeRequirementsMet = isLegalAge || isRegistered;
	console.log("The result of Logical OR Operator: " + SomeRequirementsMet
		);

	// Logica; NOT Operator (! - Exclamation Point)
	// Returns the opposite value
	let SomeRequirementsNotMet = !isRegistered;
	console.log("Result of Logical NOT Operator: "+ SomeRequirementsNotMet);

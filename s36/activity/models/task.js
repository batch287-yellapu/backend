// Contains WHAT objects are need in our API

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	status: {
		type: String,
		default: "pending"
	},
	name: String

});

// "module.exports" is a way for Node JS to treat this valua as a "package" that can be used by other files

module.exports = mongoose.model("Task", taskSchema);
const express = require("express");

const router = express.Router();

const taskController = require("../controllers/taskController");

// [ SECTION ] Routes

// Route to get a task
router.get("/:id", (req, res) => {

	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to update status of a task
router.put("/:id/complete", (req,res) => {

	taskController.updateTask(req.params.id).then(resultFromController => res.send(resultFromController));

});


module.exports = router;
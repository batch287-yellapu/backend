const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute")

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true	}));

// Setup the Database

	// Connecting to MongoDB Atlas
	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.bjkbdze.mongodb.net/s36-activity", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	mongoose.connection.on("error", console.error.bind(console, "connection error"));

	mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

	// Server listening
	app.listen(port, () => console.log(`Currently listening to port ${port}`));

	app.use("/tasks", taskRoute);
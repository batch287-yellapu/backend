const Task = require("../models/task");

// Controller to get a task

module.exports.getTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false

		} else {
			return result;
		}

	});
};

// Controller creating a task

module.exports.createTask = (requestBody) => {

	let newTask = new Task({

		name: requestBody.name
	})

	return newTask.save().then((task, error) => {

		if(error){
			console.log(error);
			return false

		} else {
			return task
		};
	});
};

// Controller updating the status of a task

module.exports.updateTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err);
			return false;
		}

		result.status = "complete";

		return result.save().then((updatedTask, saveErr) => {

			if(saveErr){

				console.log(saveErr); 
				return false;

			} else {

				return updatedTask;
			};
		});
	});
};


let trainerObject = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){console.log("Pikachu! I choose you!");}
};
console.log(trainerObject);
console.log("Result of dot notation:");
console.log(trainerObject.name);
console.log("result of square bracket notation:");
console.log(trainerObject['pokemon']);
console.log("Result of talk method");
trainerObject.talk();

function Pokemon (name, level){
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;
	
	this.tackle = function(targetPokemon){
		console.log(this.name + " tackeled " + targetPokemon.name);
		targetPokemon.health = targetPokemon.health - this.attack;
		console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);
		if(targetPokemon.health <= 0){
			targetPokemon.faint();
		}
		console.log(targetPokemon);
	}
	this.faint = function(){
		console.log(this.name +" fainted")
	}
};

let pikachu = new Pokemon('Pikachu', 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);
console.log("Hello World?");

// [SECTION] Functions

	// [SUB-SECTION] Prameters and Arguments

	// Functions in JS are lines/blocks of codes that tell our device/application/browser to perform a certain task called/invoked/triggered.

	function printInput(){
		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname);
	};

	// printInput();

	// For other cases, functions can also process data directtly passed into it instead of relying on Global variable and prompt().

	// name123 ONLY ACTS LIKE A VARIABLE

	function printName(name123){  //name123 is the parameter

		// This one shoud work as well:
			// let name = name123;
			// console.log("Hi, " + name);

			console.log("Hi, " + name123);
	};

		// console.log(name123); // This one is error as it is not defined.

	printName("Juana");	// "Juana" is argument.

	// You can directly pass data into function. The function can then call/use that data which is referred as "name123" within the function.

	printName("Lini");

	// When the "printName()" function is called again, it stores the value of "Lini" in the parameter "name123" then uses it to print a message.

	let sampleVariable = "Yui";

	printName(sampleVariable);

	// Function arguments cannot be used by function if there are no parameters provided within the function.

	printName();  // It will be Hi, undefined

	function checkDivisibiltyBy8(num){
		let remainder = num % 8;
		console.log("The remainder of "+ num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num+ " divisible by 8?");
		console.log(isDivisibleBy8);
	};

	checkDivisibiltyBy8("16");
	checkDivisibiltyBy8(52);

	// you can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

// Functions as Arguments

	// Function parameters can also accept other function as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message is printed.");
	};

	// function invokeFunction(argumentFunction){
	// 	argumentFunction();
	// };

	function invokeFunction(iAmNotRelated){
		// console.log(iAmNotRelated);
		iAmNotRelated();
		argumentFunction();
	};

	invokeFunction(argumentFunction);

	// Adding and removing the paranthese "()" impacts the output of JS heavily.

	console.log(argumentFunction);
	// Will provide information about a function in the console using console.log();

// Using multiple parameters

	// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeding order.

	function createFullName(firstName, middleName, lastName){
		console.log(firstName+ " "+ middleName+ " "+lastName);
		console.log(middleName+ " " + lastName+ " "+firstName);
	};

	createFullName("Mike", "Kel", "Jordan");
	createFullName("Mike", "Jordan");
	createFullName("Mike", "Kel", "Jordan", "Mike");

	// In JS, provoding more/less arguments than the expected parameters will not return an error.

	// Using Variable as Arguments

	let firstName = "Dwayne";
	let secondName = "The Rock";
	let lastName = "Johnson";

	createFullName(firstName, secondName, lastName);

// [SECTION] The Return Statement

	// The return statement allows us to output a value from a function to be passed to the line/block of code that invokef/called the function.

	function returnFullName(firstName, middleName, lastName){
		console.log("This message is from console.log"); // This is printed
		return firstName + " " + middleName + " " + lastName;
		console.log("This message is from console.log"); //This is not printed
	};

	let completename = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completename);

	// In our example, the "returnFullName" function was invoked called in the same line as declaring a variable.

	// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable.

	// Notice that the console.log after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.

	// In this example comsole.log() will print the returned value of the returnFullName() function.
	console.log(returnFullName(firstName, secondName, lastName));
	// console.log(returnFullName(firstName, secondName, "something")); also works

	// You can also create a variable inside the function to contain the result and return that variable instead.

	function retunAddress(city, country){
		let fullAddress = city + ", " + country;
		return fullAddress;
	};

	let myAddress = retunAddress("Manila city" , "Philippines");
	console.log(myAddress);

	// On the other hand. when a function only has console.log() to display, its result will be undefined instead.

	function printPlayerInfo(username, level, job){
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);
		// return "something";
	};

	let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
	console.log(user1);

	// Returns undefined because printPlayerInfo return nothing. It only console.log the details.

	// You cannot save any value from printPlayerInfo() because it doesnot return anything.

	
const express = require("express")

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Mock Database 
let users = [
	{
		"username": "Brandon",
		"password": "brandon1234"
	},
	{
		"username": "Jobert",
		"password": "jobert1234"
	},

	{
		"username": "Johndoe",
		"password": "Johndoe1234"
	}
];

app.get("/home", (request, response) => {

	response.send("Welcome to the home page");
});

app.get("/users", (request, response) => {

	response.send(users);
});

app.delete("/delete-user", (request, response) => {

	let message;

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){

			users.splice(i, 1);
			message = `User ${request.body.username} has been deleted.`;

			console.log("Newly updated database:");
			console.log(users);
			break;

		} else {

			message = "User not found!";
		}
	} 

	response.send(message);
})

app.listen(port, () => console.log(`Server is currently running at port ${port}`));
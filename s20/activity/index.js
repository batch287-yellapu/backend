let number = prompt("Enter a number");
console.log("The number provided by you is " + number + ".");
for (let number1 = number; number1 >=0; number1--){
	if(number1 <= 50){
		console.log("The current value is at "+ number1 + ". Terminating the loop.")
		break;
	};

	if(number1%10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	};

	if(number1%5 ===0){
		console.log(number1);
	};
};

let word = "supercalifragilisticexpialidocious";
let consonentWord = "";
for (let i=0; i<word.length; i++){
	if(word[i] == "a"||
	 	word[i] == "e" ||
	  	word[i] == "i"||
	  	word[i] == "o"||
	    word[i] == "u"){
		continue;
	}else{
	consonentWord += word[i];
	};
}

console.log(word);
console.log(consonentWord);
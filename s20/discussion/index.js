console.log("Hello world?");

// [SECTION] While Loop

	// A while loop takes in an expression/condition
	// If the condition evaluates to truw, the statements in the code block will run

	/*
	Syntax:
		While(expression/condition){
			ststement;
		};
	*/

	function whileLoop(){

		let count = 5;

		// While the value of count is not equal to 0.
		while(count !== 0){

			// The current value of count is printed out.
			console.log("While: " + count);

			// Infinite loop, our program being written without the end.
			// Make sure that expression/conditions in loops have their corresponding increment/decrement operators to stop the loop.
			count--;

			// Note:
			// Make sure that the corresponding increment/decrement is relevant to the condition/expression
		};
	}

	// Another Example

	function gradeLevel(){
		let grade = 1;

		while(grade <= 5){
			console.log("I am a grade " + grade + " student!");

			grade ++;
		};
	}

// [SECTION] Do While Loop

	// A do-while loop works alot like a while loop, but unlike while loops, do-while loops guarantee that the code will be executed at least once.

	/*
	Syntax:
		do {
			statement
		}while (expression/condition)
	*/

	function doWhile(){
		let count = 20;

		do {
			console.log("What ever happens, I will be here!");
			count--;
		} while (count>0);
	};


	// Another Example
	function secondDoWhile(){
		let number = Number(prompt("Give me a number"));

		do {

			// The current value of number is printed.
			console.log("Do While: "+ number);

			// Increases the value of the number by 1 after every iteration to stop the loop when it reaches the 10 or greater
			// number = number + 1;
			number += 1;

			// Provide a number of 10 or greater will run the code block once and will stop the loop.
		}while (number < 10);
	}


// [SECTION] For Loop

	// A for loop is more flexible than while and do-while loop.
	// For loop consists of three parts:
		// 1. The "Initialization" value that will track the progression of the loop
		// 2. The "expression/condition" that will be evaluated which will determine whether the loop will run one or more times or not.
		// 3. The "finalExpression" indicates how to advance the loop/how the variables will behave.

	/*
	Syntax:
		for (initialization ; expression/consition; finalExpression){
			statement
		}
	*/
	
	function forLoop(){
		for (let count = 10; count <= 20; count++){
			console.log("You are currently: "+ count);
		};
	}

	// [SUB-SECTION] Loops for letters

	let myString = "alexander";
	console.log(myString);
	console.log(myString.length);

		// console.log(myString[0]);
		// console.log(myString[1]);
		// console.log(myString[2]);
		// console.log(myString[3]);
		// console.log(myString[4]);

		let x=0;

		// We create a loop that will print out the individual letters of the myString variable
		for(x ; x < myString.length; x++){

			// The current variable of myString is printed out using it's index value.
			console.log(myString[x])
		};

	// Mini-Activity

	// let myName = prompt("Enter your name").toLowerCase();
	let myName = "linisha";
	console.log(myName[1]);
	myName[1] == "z";
	let myNewName = "";

	for(x= 0; x < myName.length; x++){
		if(myName[x] == "a"|| myName[x] == "e" || myName[x] == "i"|| myName[x] == "o"|| myName[x] == "u"){
			// myName[x] = "0";
			// console.log(0);
			myNewName += "0";
		} else {
			// console.log(myName[x]);
			myNewName += myName[x];};
	}

	console.log(myName);
	console.log(myNewName);

// [SECTION] Continue and Break Statements

	// The "continue" statement allows the code to get to the next iteration of the loop without finishing the execution of all statements in a code block
	// The "break" statement is used to terminate the current loop once a match has been found

	for (let count = 0; count <=20; count++){

		// if remainder is equal to 0
		if(count % 2 === 0){
			// Tells the code to continue to the next iteration of the loop;
			// This ignores all statements located after the continue statement;
			continue;
		};

		// The current value of number is printed out if the remaindr is not equal to 0;
		console.log("Continue and Break: " + count);

		// If the current value of count is greater than 10
		if(count > 10){

			// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20;
			// number values after 10 will no longer be printed
			break
		};
	};

	// Another Example

		let name = "Alexandro";

		for (let i=0; i<name.length; i++){
			console.log(name[i]);

			// If the vowel is equal to a, continue to the next iteration of the loop
			if (name[i].toLowerCase() === 'a'){
				console.log('Continue to the next iteration');
				continue;
			};

			// If the current letter is equal to d, stop the loop
			if (name[i] == 'd'){
				break;
			};
		}

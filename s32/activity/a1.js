let http = require("http");

http.createServer(function(request, response){

	if(request.url == "/" && request.method == 'GET'){

		response.writeHead(200, {'content-Type': 'text/plain'});
		response.end("Welcome to booking system");
	}

	if(request.url == "/profile" && request.method == 'GET'){

		response.writeHead(200, {'content-Type': 'text/plain'});
		response.end("Welcome to your profile");
	}

	if(request.url == "/courses" && request.method == 'GET'){

		response.writeHead(200, {'content-Type': 'text/plain'});
		response.end("Here's our courses available");
	}

	if(request.url == "/addCourse" && request.method == 'POST'){

		response.writeHead(200, {'content-Type': 'text/plain'});
		response.end("Add course to our resources");
	}

}).listen(4000);
console.log('Server is running at localhost: 4000');
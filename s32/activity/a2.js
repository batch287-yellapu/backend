let http = require("http");

http.createServer(function(request, response){

	if(request.url == "/updateCourse" && request.method == 'PUT'){

		response.writeHead(200, {'content-Type': 'text/plain'});
		response.end("Update a course to our resources");
	}

	if(request.url == "/archiveCourse" && request.method == 'DELETE'){

		response.writeHead(200, {'content-Type': 'text/plain'});
		response.end("Archive courses to our resources");
	}

}).listen(4000);
console.log('Server is running at localhost: 4000');
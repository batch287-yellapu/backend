// Fetch request to retrieve all to-do list items
let titles = [];
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((response) => response.json())
  .then((data) => {
    // Extracting titles using map method
    titles.push(data.map((item) => item.title));
    console.log(titles);
  });

  // Fetch request to retrieve a single to-do list item
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
    const { title, completed } = data;
    console.log(`The item "${title}" on the list has a status of ${completed}`);
  });
  
// POST
  // Fetch request to create a to-do list item
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Created To Do List Item',
    completed: false,
    userId: 1
  })
})
  .then((response) => response.json())
  .then((data) => console.log(data));

// PUT
// Fetch request to update a to-do list item
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Updated To Do List Item',
    description: 'To update the my to do list with a different data structure',
    status: 'Pending',
    dateCompleted: 'Pending',
    userId: 1
  })
})
  .then((response) => response.json())
  .then((data) => console.log(data));

// PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    completed: false,
    title: 'delectus aut autem',
    // description: 'To update the my to do list with a different data structure',
    status: 'Complete',
    dateCompleted: '07/09/21',
    userId: 1
  })
})
  .then((response) => response.json())
  .then((data) => console.log(data));

// DELETE
  fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: 'DELETE'
})
  .then((response) => {
    console.log("Item deleted successfully");
  });
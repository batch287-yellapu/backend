const http = require('http');

// creates a variable 'port' to store the port number
const port = 4004;

// Creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request, response) => {

	// Accessing the 'greeting' routes returns a message of "Welcome to the server! This is currently running at local host: 4004."
	if (request.url == '/greeting'){

		response.writeHead(200, {'content-Type': 'text/plain'})
		response.end('Welcome to the server! This is currently running at local host: 4004.')

	// Accessing the 'homepage' routes returns a message of "Welcome to the homepage! This is currently running at local host: 4004."
	}else if (request.url == '/homepage'){

		response.writeHead(200, {'content-Type': 'text/plain'})
		response.end('Welcome to the homepage! This is currently running at local host: 4004.')

	// All other routes will return a message of "Page not available!"
	}else {

		// Set a Status code for the response - 404 means Not Found
		response.writeHead(404, {'content-Type': 'text/plain'})
		response.end('Page not avilable!')
	}
})

// Uses the "server" and "port" variables created above.
server.listen(port);

// When server is running, console will print the message:
console.log(`Server is now accessible at localhost: ${port}.`);
